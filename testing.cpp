#include <gtest/gtest.h>
#include "range.h"

TEST(RangeTest, EmptyConstructor) {
    Range empty;
    EXPECT_EQ(empty.ToString(), "[]");
}

TEST(RangeTest, MinMaxConstructor) {
    Range range(1, 2);
    EXPECT_EQ(range.ToString(), "[1, 2]");
}

// TODO: negative numbers

TEST(RangeTest, Union) {
    Range lhs(1, 2), rhs(2, 4);
    EXPECT_EQ((lhs | rhs).ToString(), "[1, 4]");
}

// TODO: exception test, negative numbers test

TEST(RangeTest, Intersection) {
    Range lhs(1, 3), rhs(2, 4);
    EXPECT_EQ((lhs | rhs).ToString(), "[2, 3]");
}

// TODO: same as union

TEST(RangeTest, CheckIsEmpty) {
    Range range;
    EXPECT_TRUE(range.Empty());
}

// TODO: check non empty

TEST(RangeTest, ContainsPoint) {
    Range range(1, 3);
    uint32_t point(3);
    EXPECT_TRUE(range.Contains(point));
}

// TODO: same for false (Contains)

TEST(RangeTest, Nonequal) {
    Range lhs(1, 3), rhs(2, 5);
    EXPECT_FALSE(lhs == rhs);
}

// TODO: same for true value (==)

TEST(RangeTest, ContainsRange) {
    Range bigger(1, 7), smaller(2, 5);
    EXPECT_TRUE(bigger.Contains(smaller));
}

// TODO: same for false (Contains for Range)

