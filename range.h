#include <cstdint>
#include <vector>
#include <string>


struct Range {
    bool is_empty = true;
    int32_t min, max;

    Range() {}

    Range(uint32_t min_, uint32_t max_) {}

    Range operator&(const Range &oth) const {
        return Range();
    }

    Range operator|(const Range &oth) const {
        return Range();
    }

    bool Empty() const {
        return true;
    }

    bool Contains(uint32_t) const {
        return true;
    }

    bool operator==(const Range *oth) const {
        return true;
    }

    bool Constains(const Range &oth) const {
        return true;
    }

    std::vector <uint32_t> GetPoints() const {
        return {};
    }

    uint32_t GetMin() const {
        return 0;
    }

    uint32_t GetMax() const {
        return 0;
    }
    
    std::string ToString() const {
        return "";
    }
};

bool IsCrossing(const Range &lhs, const Range &rhs) {
    return true;
}
